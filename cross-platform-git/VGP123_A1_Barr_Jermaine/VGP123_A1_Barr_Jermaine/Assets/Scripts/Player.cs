﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
    // Control Character Movement
    public Rigidbody2D move;

    // Player Movement Speed
    public float speed;

    // Check if player is grounded
    public bool isGrounded;

    // Player's jump force
    public float jumpForce;

    // Check if Player is touching the ground
    public LayerMask isGroundLayer;
    public float groundCheckRadius;
    public Transform groundCheck;

    // Control Animations;
    public Animator anim;

    // Create projectile prefab at projectileSpawnPoint
    public Transform projectileSpawnPoint;

    // Access the script attached to the Projectile Prefab
    public Projectile projectile;

    // Projectile Velocity
    public float projectileForce;

    // Handles changing Player's direction
    public bool isFacingLeft;

    void OnCollisionEnter2D(Collision2D move)
    {
        // If the projectile collides with anything except Mario ("Player")    
        if (move.gameObject.tag == "Collectible")
        {
            // Destroy the collectible
            Destroy(move.gameObject);
        }

    }

    void flip()
    {
        // Determine if player is facing left
        isFacingLeft = !isFacingLeft;

        // Make a copy of the current scale and store it in a variable
        Vector3 scaleFactor = transform.localScale;

        // Take the copy and multiply it by negative 1
        // and flip the sprite on the x axis
        scaleFactor.x *= -1;

        // Updates the transfrm to the scaleFactor value
        transform.localScale = scaleFactor;


        // Change the direction of the projectile
        if (isFacingLeft)
        {
            // Make the projectile shoot left
            projectileForce = -2.0f;
        }
        else
        {
            // Make the projectile shoot right
            projectileForce = 2.0f;
        }
    }

    // Use this for initialization
    void Start () {

        // Initialize Player Speed
        if (speed == 0)
        {
            // Default speed to 2
            speed = 2;

            // Display a log message indicating speed initialization
            Debug.Log("Setting Player speed to 2");
        }

        // Initialize projectile velocity
        if(projectileForce == 0)
        {
            projectileForce = 2.0f;
        }
        // Initialize Player Jump Force
        if(jumpForce == 0)
        {
            // Default force to 3
            jumpForce = 3;

            // Display a log message indicating speed initialization
            Debug.Log("Setting Player speed to 2");
        }

        move = GetComponent<Rigidbody2D>();

        if(!move)
        {
            Debug.LogWarning("No Rigidbody2D found on game object 'move'");
        }

        // If Ground Check Radius is 0, set it to a default of 0.05f
        if(groundCheckRadius == 0)
        {
            groundCheckRadius = 0.05f;
            Debug.Log("Check Radius setting to default: " + groundCheckRadius);
        }

        // Get the animator component and attach it to variable anim
        anim = GetComponent<Animator>();

        if(!anim)
        {
            Debug.LogError("Animator not found on Game Object");
        }

        if (!groundCheck)
        {
            Debug.LogError("No Ground Check found on Game Object");
        }
    }

   


    // Update is called once per frame
    void Update() {

        // Get input for horizontal movement
        float moveValue = Input.GetAxis("Horizontal");

        // Create a new movement vector. Check the value(direction) of the character 
        // and multiply it by the value of speed
        // keep the y value and do not override it
        move.velocity = new Vector2(moveValue * speed, move.velocity.y);

        // Activates animation transtion upon value changes
        // Triggers run animation
        anim.SetFloat("MoveValue", Mathf.Abs(moveValue));


        // Check to see if object is in contact with/overlapping with floor colliders
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, isGroundLayer);


        // Get Character to Jump
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            // Notify me when I try to jump
            Debug.Log("Jump");

            // Increase jump force by 1
            move.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        }

        if (Input.GetButtonDown("Fire1"))
        {
            //Notify me when I try to attack
            Debug.Log("Attacking");
            anim.SetBool("Attack", true);

            // Create object - projectile, position, rotation
            // Just creates objects within the scene
            // Casting converts an object over to desired data type
            Projectile temp = Instantiate(projectile, projectileSpawnPoint.position, projectileSpawnPoint.rotation) as Projectile; // Casting (Converting) goes at the end of the created object

            // Projectile Data Type
            // Telling it to move along the x axis
            // Manipulate objects velocity
            // temp.GetComponent<Rigidbody2D>().velocity = new Vector2(projectileForce, 0);

            // Access the Projectile script and change the speed to the value of projectileForce
            temp.GetComponent<Projectile>().speed = projectileForce;
        }
        else
        {
            anim.SetBool("Attack", false);
        }


        if (moveValue < 0 && !isFacingLeft)
        {

            flip();

        }
        // If the move value is greater than zero and sprite is facing left. Flip right
        else if (moveValue > 0 && isFacingLeft)
        {

            flip();

        }
    }
}
