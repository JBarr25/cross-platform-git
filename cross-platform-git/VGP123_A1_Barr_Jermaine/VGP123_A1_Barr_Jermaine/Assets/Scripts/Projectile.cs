﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

    // Projectile Speed
    public float speed;

    // Destroy projectile after certain time
    public float lifeTime;

    //Projectile collision detection
    void OnCollisionEnter2D(Collision2D projectile)
    {

        // If the projectile collides with anything except the Player
        if(projectile.gameObject.tag != "Player")
        {
            //Destroy the Projectile
            Destroy(gameObject);
			//new comment
        }
    }
	// Use this for initialization
	void Start () {
	
        // Set default lifeTime value
        if(lifeTime == 0)
        {
            // Set lifeTime to 1.5 seconds
            lifeTime = 1.5f;
        }

        // Move the projectile along x axis
        // Maintain y axis
        GetComponent<Rigidbody2D>().velocity = new Vector2(speed, 0);

        // Destroy projectile after time has been reached
        Destroy(gameObject, lifeTime);

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
